using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class box : MonoBehaviour
{

	private List<string> furniture;

    // Start is called before the first frame update
    void Start()
    {
		furniture = new List<string>{};
        
		var trans = GameObject.Find("Möbler").transform;
		var opts = GetComponent<TMP_Dropdown>();
		for (int i = 0; i < trans.childCount; ++i) {
			// Debug.Log("möbel[" + i + "] = " + trans.GetChild(i));
			furniture.Add(trans.GetChild(i).name);
		}
		opts.AddOptions(furniture);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void OnValueChanged(int i) {
		Debug.Log("Changed: " + furniture[i]);
	}
}
