using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MouseInput : MonoBehaviour
{

	private Global globals;
	public float torque = 1000.0f;
	public float moveForce = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        globals = GameObject.Find("Globals").GetComponent<Global>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
		/* TODO a single click should cancel all current forces of
		 * that type */
		if (globals.grabbed) {
			if (Input.GetKey(KeyCode.Mouse0)) {
				/** Moving */
				// TODO fix motion relative camera
				// var x = Input.GetAxis("Mouse X");
				// var y = Input.GetAxis("Mouse Y");
				// var point = new Vector3(x, y, 0);
				// var point = new Vector3(
				// 		Event.current.mousePosition.x,
				// 		Event.current.mousePosition.y,
				// 		0);

				var cam = GetComponent<Camera>();
				var mousePointer = GameObject.Find("GraphicalMousePointer");

				Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~(1<<16))) {
					var point = hit.point;
					mousePointer.transform.position = point;
					point.y = 0;
					var rbody = globals.grabbed.GetComponent<Rigidbody>();
					// rbody.MovePosition(point);
					// var force = Vector3.MoveTowards(rbody.position, point, 1f);
					var force = rbody.position - point;
					Debug.Log("Force: " + force);
					rbody.AddForce(-10 * force);
					// globals.grabbed.velocity = Vector3.zero;
				}

				// globals.grabbed.AddRelativeForce(force);
			} else if (Input.GetKey(KeyCode.Mouse1)) {
				/** Rotating */
				var turn = Input.GetAxis("Mouse X");
				var force = Vector3.up * turn * torque;
				Debug.Log("Torque: " + force);
				globals.grabbed.AddRelativeTorque(force);
				// globals.grabbed.GetComponent<Rigidbody>().MoveRotation(world);
			} else {
				globals.grabbed = null;
			}

			// globals.grabbed.eulerAngles.x = 0;
			// globals.grabbed.eulerAngles.z = 0;

		} else if (globals.highlighted) {
			if (Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1)) {
				globals.grabbed = globals.highlighted;
			}
		}
    }
}
