using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlight : MonoBehaviour
{
    private Color startcolor;
    private Global globals;

    // Start is called before the first frame update
    void Start()
    {
        globals = GameObject.Find("Globals").GetComponent<Global>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseEnter()
    {
        startcolor = GetComponent<Renderer>().material.color;
        GetComponent<Renderer>().material.color = Color.yellow;

        globals.highlighted = GetComponent<Rigidbody>();
        globals.currentlyOver = true;
    }

    void OnMouseExit() 
    {
        globals.highlighted = null;
        globals.currentlyOver = false;
        GetComponent<Renderer>().material.color = startcolor;
    }

    void OnMouseMove() {
                var change = Input.GetAxis("Mouse X");
                // Debug.Log("Change: "+ change);
                // Input.GetAxis("Mouse Y");
    }

    /*
    void OnMouseDrag()
    {
        // Debug.Log("mode:" +mode);
        switch (mode) {
            case Mode.Drag:
                var camera = GameObject.Find("Camera").GetComponent<Camera>();
                var target = camera.ScreenToWorldPoint(Input.mousePosition);
                var force = Vector3.MoveTowards(transform.position, target, scale);
                force.y = 0;
                Debug.Log(force);
                GetComponent<Rigidbody>().AddForce(force);
                break;

            case Mode.Rotate:
                var change = Input.GetAxis("Mouse X");
                // Debug.Log("Change: "+ change);
                // Input.GetAxis("Mouse Y");
                break;
        }
    }
    */
}
