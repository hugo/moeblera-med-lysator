using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class keyboard : MonoBehaviour
{

	private GameObject sphere;

    // Start is called before the first frame update
    void Start()
    {
		sphere = GameObject.Find("CameraFocus");        
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetButtonDown("Fire
		sphere.transform.Rotate(0, - Input.GetAxis("Horizontal"), 0);
		sphere.transform.Rotate(Input.GetAxis("Vertical"), 0, 0);
    }

	void OnKeyDown(KeyDownEvent ev) {
		/*
		Debug.Log("KeyDown:" + ev.keyCode);
		Debug.Log("KeyDown:" + ev.character);
		Debug.Log("KeyDown:" + ev.modifiers);
		*/
	}
}
